import App from '~/components/App.svelte';

const app = new App({
  target: document.body,
  props: {
    appName: 'NanoClock',
  },
});

window.app = app;

export default app;
